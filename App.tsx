import 'react-native-gesture-handler';
import React, {useState} from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Navbar from './src/components/Navbar';
import Sidebar from './src/components/Sidebar';

import Calendar from './src/components/Calendar';

import {RootStore, StoreContext} from './src/stores/RootStore';

import iconFont from 'react-native-vector-icons/Fonts/FontAwesome.ttf';

const iconFontStyles = `@font-face {
  src: url(${iconFont});
  font-family: FontAwesome;
}`;

if (Platform.OS === 'web') {
  const style = document.createElement('style');
  style.type = 'text/css';
  if (style.styleSheet) {
    style.styleSheet.cssText = iconFontStyles;
  } else {
    style.appendChild(document.createTextNode(iconFontStyles));
  }

  document.head.appendChild(style);
}

const Stack = createStackNavigator();

const WithBars = ({Component, ...props}) => {
  const [sidebar, setSidebar] = useState(false);
  return (
    <View style={styles.componentContainer}>
      <Navbar setSidebar={setSidebar} />
      {sidebar ? <Sidebar setSidebar={setSidebar} /> : null}
      <Component {...props} />
    </View>
  );
};

const App = () => {
  return (
    <StoreContext.Provider value={new RootStore()}>
      <View style={styles.container}>
        <NavigationContainer>
          <Stack.Navigator
            headerMode={'none'}
            screenOptions={{cardStyle: {backgroundColor: 'transparent'}}}>
            <Stack.Screen name="Home">
              {props => <WithBars Component={Calendar} {...props} />}
            </Stack.Screen>
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    </StoreContext.Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'android' || Platform.OS === 'ios' ? 40 : 0,
    flex: 1,
    justifyContent: 'flex-start',
  },
  componentContainer: {
    position: 'relative',
  },
});

export default App;
