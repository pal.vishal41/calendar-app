import React from 'react';
import { GoogleLogin } from 'react-google-login';

const Authentication = () => {
  const responseGoogle = (response) => {
    console.log(response);
  };

  return (
    <GoogleLogin
      clientId="377956810382-u63bof4ohpgmt4aa75ro0savu3hqslhd.apps.googleusercontent.com"
      buttonText="Login"
      onSuccess={responseGoogle}
      onFailure={responseGoogle}
      cookiePolicy={'single_host_origin'}
      redirectUri={'http://localhost:8081/redirect'}
    />
  );
};

export default Authentication;
