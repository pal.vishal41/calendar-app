import React, {useState, useContext} from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import moment from 'moment';
import {observer} from 'mobx-react-lite';

import {StoreContext} from '../stores/RootStore';

const daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

const Row = ({children}) => {
  return <View style={styles.row}>{children}</View>;
};

const HeadCell = ({children}) => {
  return <View style={styles.headCell}>{children}</View>;
};

const Cell = ({children}) => {
  return <View style={styles.cell}>{children}</View>;
};

const CalendarRow = ({children}) => {
  return <View style={styles.calendarRow}>{children}</View>;
};

const Calendar = observer(() => {
  const {selectedDate, selectedMonth, selectedYear} = useContext(StoreContext).calendarStore;

  let calendar = [];

  const firstDay = moment([selectedYear, selectedMonth, 1]).day();
  if (firstDay > 0) {
    let tempDate =
      daysInMonths[(((selectedMonth - 1) % 12) + 12) % 12] - firstDay + 1;
    for (let i = 0; i < firstDay; i++) {
      calendar.push({date: tempDate, month: 'prev'});
      tempDate++;
    }
  }

  for (let i = 1; i <= daysInMonths[selectedMonth]; i++) {
    calendar.push({date: i, month: 'cur'});
  }

  const nextLeft = 35 - calendar.length;

  for (let i = 1; i <= nextLeft; i++) {
    calendar.push({date: i, month: 'next'});
  }

  const currentMoment = moment();

  return (
    <View style={styles.container}>
      <Row>
        {daysOfWeek.map(day => (
          <HeadCell key={day}>
            <Text style={styles.date}>{day}</Text>
          </HeadCell>
        ))}
      </Row>
      <CalendarRow>
        {calendar.map(d => (
          <Cell key={d.month + d.date}>
            {d.month === 'cur' ? (
              d.date === currentMoment.date() &&
              selectedMonth === currentMoment.month() &&
              selectedYear === currentMoment.year() ? (
                <View style={[styles.date, styles.curDate]}>
                  <Text style={styles.curDateText}>{d.date}</Text>
                </View>
              ) : (
                <Text style={[styles.date, styles.curMonth]}>{d.date}</Text>
              )
            ) : (
              <Text style={[styles.date, styles.nonCurMonth]}>{d.date}</Text>
            )}
          </Cell>
        ))}
      </CalendarRow>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height * 0.9,
    width: '100%',
    display: 'flex',
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    height: '5%',
  },
  headCell: {
    width: '14%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'rgba(0,0,0,.1)',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  cell: {
    height: '20%',
    padding: 2,
    width: '14%',
    display: 'flex',
    alignItems: 'center',
    borderColor: 'rgba(0,0,0,.1)',
    borderRightWidth: 0.5,
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderTopWidth: 0.5,
    borderStyle: 'solid',
  },
  calendarRow: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    height: '95%',
  },
  date: {
    padding: 5,
    width: 'auto',
    textAlign: 'center',
    fontSize: 10,
  },
  nonCurMonth: {
    color: 'rgba(0,0,0,.4)',
  },
  curMonth: {
    color: '#000',
  },
  curDate: {
    backgroundColor: 'blue',
    borderTopLeftRadius: 100,
    borderTopRightRadius: 100,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    overflow: 'hidden',
  },
  curDateText: {
    color: '#fff',
    fontSize: 10,
  },
});

export default Calendar;
