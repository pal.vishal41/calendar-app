import React, {useContext} from 'react';
import {View, Text, StyleSheet, Platform, Button} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {observer} from 'mobx-react-lite';
// import {
//   GoogleSignin,
//   GoogleSigninButton,
//   statusCodes,
// } from '@react-native-google-signin/google-signin';

import Authentication from './Authentication';

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

import {StoreContext} from '../stores/RootStore';

// const config = {
//   issuer: 'https://accounts.google.com/o/oauth2/v2/auth',
//   clientId: '377956810382-u63bof4ohpgmt4aa75ro0savu3hqslhd.apps.googleusercontent.com',
//   redirectUrl: 'http://localhost:8081',
//   scopes: [],
// };

// GoogleSignin.configure({
//   scopes: [], // what API you want to access on behalf of the user, default is email and profile
//   webClientId: '377956810382-u63bof4ohpgmt4aa75ro0savu3hqslhd.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
//   offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
//   // hostedDomain: '', // specifies a hosted domain restriction
//   // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
//   // forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
//   // accountName: '', // [Android] specifies an account name on the device that should be used
//   // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
//   // googleServicePlistPath: '', // [iOS] optional, if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
// });

const Navbar = observer(({setSidebar}) => {
  const {calendarStore} = useContext(StoreContext);
  const googleLogin = async () => {
    try {
      // await GoogleSignin.hasPlayServices();
      // const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
    } catch (error) {
      console.log(error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  return (
    <View style={styles.container}>
      {Platform.OS === 'android' || Platform.OS === 'ios' ? (
        <Icon
          name="bars"
          size={20}
          color="#000"
          onPress={() => setSidebar(true)}
        />
      ) : (
        <View style={styles.brand}>
          <Text style={styles.brandText}>mCalendar</Text>
          <Authentication />
        </View>
      )}
      <Icon
        name="chevron-left"
        size={15}
        color="#000"
        onPress={() => {
          if (calendarStore.selectedMonth === 0) {
            calendarStore.changeYear(calendarStore.selectedYear - 1);
          }
          calendarStore.changeMonth(
            (((calendarStore.selectedMonth - 1) % 12) + 12) % 12,
          );
        }}
      />
      <Text>
        {monthNames[calendarStore.selectedMonth]} {calendarStore.selectedYear}
      </Text>
      <Icon
        name="chevron-right"
        size={15}
        color="#000"
        onPress={() => {
          if (calendarStore.selectedMonth === 11) {
            calendarStore.changeYear(calendarStore.selectedYear + 1);
          }
          calendarStore.changeMonth(
            (((calendarStore.selectedMonth + 1) % 12) + 12) % 12,
          );
        }}
      />
      <Button onPress={googleLogin} title="Login" />
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    height: '7%',
    width: '100%',
    padding: 15,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomColor: 'rgba(0,0,0,.1)',
    borderBottomWidth: 1,
  },
  brand: {
    marginRight: 20,
  },
  brandText: {
    fontSize: 30,
  },
});

export default Navbar;
