import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Platform,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native';

const Sidebar = ({setSidebar}) => {
  let x = new Animated.Value(-400);
  const slide = () => {
    Animated.spring(x, {
      toValue: 0,
      useNativeDriver: true,
      bounciness: 0,
      speed: 20,
    }).start();
  };

  const slideBack = () => {
    Animated.spring(x, {
      toValue: -400,
      useNativeDriver: true,
      bounciness: 0,
      speed: 20,
    }).start();
  };

  const closeDrawer = () => {
    slideBack();
    setTimeout(() => {
      setSidebar(false);
    }, 200);
  };

  useEffect(() => {
    slide();
  }, []);

  return (
    <TouchableWithoutFeedback onPress={() => closeDrawer()}>
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={null}>
          <Animated.View
            style={[
              styles.drawer,
              {
                transform: [
                  {
                    translateX: x,
                  },
                ],
              },
            ]}>
            <Text>Sidebar</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
  },
  drawer: {
    height: '100%',
    width: Platform.OS === 'ios' || Platform.OS === 'android' ? '70%' : '20%',
    backgroundColor: '#fff',
    padding: 10,
    zIndex: 2,
  },
});

export default Sidebar;
