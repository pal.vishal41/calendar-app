import {makeAutoObservable} from 'mobx';
import moment from 'moment';

class CalendarStore {
  selectedDate: number = 0;
  selectedMonth: number = 0;
  selectedYear: number = 0;
  rootStore;
  constructor(rootStore: any) {
    makeAutoObservable(this, {rootStore: false});
    let current = moment();
    this.selectedDate = current.date();
    this.selectedMonth = current.month();
    this.selectedYear = current.year();
    this.rootStore = rootStore;
  }

  changeMonth(month: number) {
    this.selectedMonth = month;
  }

  changeYear(year: number) {
    this.selectedYear = year;
  }
}

export default CalendarStore;
