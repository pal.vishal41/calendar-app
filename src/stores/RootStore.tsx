import {createContext} from 'react';
import CalendarStore from './CalendarStore';

export class RootStore {
  calendarStore;
  constructor() {
    this.calendarStore = new CalendarStore(this);
  }
}

export const StoreContext = createContext<RootStore>();
